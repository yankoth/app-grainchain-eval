# Prueba t�cnica web developer / GrainChain
### Demo: <https://www.grainchain-eval.yankonavarro.com/>  

// Instalar dependencias  
npm install  

// Configurar la ruta a la api en la carpeta de enviroments el archivo enviroment.ts  
export const environment = {  
  production: false,  
  apiUrl: '' <--  
};  

// Probar  
ng test  

// Iniciar localmente  
ng serve --open  

Nota: Generalmente el trabajo va sobre tasks o historias de usuario y se realiza un commit/push por cada uno de estos. En este caso, por cuestiones de tiempo se hizo todo en un commit.  

