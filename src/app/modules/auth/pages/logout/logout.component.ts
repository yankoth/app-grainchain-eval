import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '@data/service/auth.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.authService.logout()
  	.subscribe()
  	.add(() => {
      this.authService.removeToken();
      this.router.navigate(['/auth/login']);
  	});
  }

}
