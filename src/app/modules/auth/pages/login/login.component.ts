import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '@data/service/auth.service';
import { User } from '@data/types/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthService) {
  	this.user = new User();
  }

  loginForm: FormGroup | any;
  loginFail: boolean = false;
  user: User;
  entering: boolean = false;

  ngOnInit(): void {
  	this.loginForm = this.formBuilder.group({
	  email: ['', Validators.compose([
        Validators.required,
        Validators.pattern('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$')
      ])],
      password: ['', Validators.required]
    });
  }

  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }

  login (event: any) {
  	this.loginFail = false;

  	if (!this.loginForm.valid) {
  	  this.loginForm.markAllAsTouched();
	  return;
  	}

  	if (this.entering) {
  		return;
  	}

  	this.entering = true;

  	this.user.email = this.email.value;
  	this.user.password = this.password.value;

  	this.authService.login(this.user)
  	.subscribe((token: string) => {
  	  this.authService.setToken(token);
      this.router.navigate(['/']);
  	}, () => {
      this.loginFail = true;
    })
    .add(() => {
      this.entering = false;
    });
  }

  keyDownForm (event: any) {
    this.loginFail = false;
  }

}
