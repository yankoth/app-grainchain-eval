import { Component, Input, OnInit } from '@angular/core';

import { EpisodeService } from '@data/service/episode.service';
import { Episode } from '@data/types/episode';

@Component({
  selector: 'app-episode-item',
  templateUrl: './episode-item.component.html',
  styleUrls: ['./episode-item.component.scss']
})
export class EpisodeItemComponent implements OnInit {

  @Input() episode: Episode = new Episode();

  constructor(private episodeService: EpisodeService) { }

  ngOnInit(): void {
  }

  formatEpisode(episode: Episode) {
  	return 'S' + ('0' + episode.season.toString()).slice(-2) + 'E' + ('0' + episode.number.toString()).slice(-2);
  }

  formatAirDate(value: string) {
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const date = new Date(value);

    return months[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
  }

}
