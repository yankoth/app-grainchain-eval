import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { CharacterService } from '@data/service/character.service';
import { Character } from '@data/types/character';

@Component({
  selector: 'app-character-item',
  templateUrl: './character-item.component.html',
  styleUrls: ['./character-item.component.scss']
})
export class CharacterItemComponent implements OnInit {

  characterCopy: Character | null = null;
  @Input() character: Character = new Character();

  updating: boolean = false;
  editCharacter: boolean = false;

  @Output() alert = new EventEmitter<any>();

  constructor(private characterService: CharacterService) { }

  ngOnInit(): void {
  }

  ucFirst(value: string) {
  	return value[0].toUpperCase() + value.substr(1);
  }

  copy (item: any) {
    // It works this way because the location property is an object and there's no date or function properties.
    // In other situation I would use lodash _.cloneDeep().
    return JSON.parse(JSON.stringify(item));
  }

  edit () {
    this.editCharacter = !this.editCharacter;

    if (this.editCharacter) {
      this.characterCopy = this.copy(this.character);
    } else {
      this.characterCopy = null;
    }
  }

  reset () {
    this.character = this.copy(this.characterCopy);
  }

  update () {
    if (this.updating) {
      return;
    }

    this.updating = true;

    this.characterService.update(this.character)
    .subscribe((data: Character) => {
      this.characterCopy = this.copy(data);
      this.notify(data, 'Character ' + this.character.name + ' was edited.', 'success');
    }, (response: any) => {
      this.notify(null, response.error, 'error');
    })
    .add(() => {
      this.updating = false;
    });
  }

  notify (character: Character | null, message: string, type: string) {
    this.alert.emit({ character, message, type });
  }

}
