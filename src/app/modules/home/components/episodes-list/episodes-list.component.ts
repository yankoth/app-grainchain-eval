import { Component, OnInit } from '@angular/core';

import { EpisodeService } from '@data/service/episode.service';
import { Episode } from '@data/types/episode';

@Component({
  selector: 'app-episodes-list',
  templateUrl: './episodes-list.component.html',
  styleUrls: ['./episodes-list.component.scss']
})
export class EpisodesListComponent implements OnInit {

  episodes: Array<Episode> = [];
  itemsPerPage: number = 50;
  page: number = 1;
  thereMore: boolean = false;

  constructor(private episodeService: EpisodeService) { }

  ngOnInit(): void {
  	this.episodeService.paginate(this.itemsPerPage, this.page)
  	.subscribe((data: Array<Episode>) => {
      this.thereMore = true;
  		this.episodes = data;
  	});
  }

  loadMore(): void {
    this.page++;
    this.thereMore = false;

    this.episodeService.paginate(this.itemsPerPage, this.page)
    .subscribe((data: Array<Episode>) => {
      if (data.length) {
        this.thereMore = true;
        this.episodes = [...this.episodes, ...data];
      }
    });
  }

}
