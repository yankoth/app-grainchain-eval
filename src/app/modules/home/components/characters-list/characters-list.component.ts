import { Component, OnInit } from '@angular/core';

import { CharacterService } from '@data/service/character.service';
import { Character } from '@data/types/character';

@Component({
  selector: 'app-characters-list',
  templateUrl: './characters-list.component.html',
  styleUrls: ['./characters-list.component.scss']
})
export class CharactersListComponent implements OnInit {

  characters: Array<Character> = [];
  itemsPerPage: number = 50;
  page: number = 1;
  thereMore: boolean = false;

  notificationDisplay: boolean = false;
  notificationTimeout: any = null;
  notificationMessage: string = '';
  notificationType: string = '';

  constructor(private characterService: CharacterService) { }

  ngOnInit(): void {
  	this.characterService.paginate(this.itemsPerPage, this.page)
  	.subscribe((data: Array<Character>) => {
      this.thereMore = true;
  		this.characters = data;
  	});
  }

  loadMore(): void {
    this.page++;
    this.thereMore = false;

    this.characterService.paginate(this.itemsPerPage, this.page)
    .subscribe((data: Array<Character>) => {
      if (data.length) {
        this.thereMore = true;
        this.characters = [...this.characters, ...data];
      }
    });
  }

  alert (options: any) {
    if (this.notificationDisplay) {
      clearTimeout(this.notificationTimeout);
    }

    if (options.character) {
      const editedIndex = this.characters.findIndex(character => character.id === options.character.id);
      Object.assign(this.characters[editedIndex], options.character);
    }

    this.notificationMessage = options.message;
    this.notificationType = options.type;
    this.notificationDisplay = true;
    this.notificationTimeout = setTimeout(() => this.notificationDisplay = false, 3000);
  }

}
