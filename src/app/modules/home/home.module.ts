import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';

import { CharactersComponent } from './pages/characters/characters.component';
import { EpisodesComponent } from './pages/episodes/episodes.component';
import { CharactersListComponent } from './components/characters-list/characters-list.component';
import { EpisodesListComponent } from './components/episodes-list/episodes-list.component';
import { CharacterItemComponent } from './components/character-item/character-item.component';
import { EpisodeItemComponent } from './components/episode-item/episode-item.component';

@NgModule({
  declarations: [CharactersComponent, EpisodesComponent, CharactersListComponent, EpisodesListComponent, CharacterItemComponent, EpisodeItemComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    SharedModule,
  ]
})
export class HomeModule { }
