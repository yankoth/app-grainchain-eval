import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/characters', pathMatch: 'full' },
  {
    path: '',
    loadChildren: () => import('@modules/home/home.module')
      .then(mod => mod.HomeModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('@modules/auth/auth.module')
      .then(mod => mod.AuthModule)
  },
  // Fallback when no prior routes is matched
  { path: '**', redirectTo: '/characters', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
