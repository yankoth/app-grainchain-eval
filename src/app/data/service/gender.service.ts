import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Gender } from '../types/gender';

const routes = {
  index: 'genders',
};

@Injectable({
  providedIn: 'root'
})
export class GenderService {

  constructor(private http: HttpClient) {}

  index(): Observable<any> {
	return this.http.get<any>(routes.index)
	  .pipe(
        map((response: any) => {
          const data = response.data as Array<Gender>;
          return data;
        })
      );
  }
}
