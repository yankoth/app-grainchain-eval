import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { User } from '../types/user';

const routes = {
  login: 'login',
  logout: 'logout',
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {}

  login(user: User): Observable<any> {
	  return this.http.post<any>(routes.login, user)
    .pipe(
      map((response: any) => {
        return response.sessionToken;
      })
    );
  }

  logout(): Observable<any> {
    return this.http.post<any>(routes.logout, {});
  }

  isAuthenticated(): boolean {
    const token: string | null = this.getToken();

    return !!token;
  }

  getToken(): string | null {
    const token: string | null = localStorage.getItem('grainchain-eval-session-token');

	  return token;
  }

  setToken(token: string): void {
    localStorage.setItem('grainchain-eval-session-token', token);
  }

  removeToken(): void {
  	localStorage.removeItem('grainchain-eval-session-token');
  }
}
