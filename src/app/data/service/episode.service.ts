import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Episode } from '../types/episode';

const routes = {
  index: 'episodes',
  paginate: (itemsPerPage: number, page: number) =>  `episodes?items_per_page=${itemsPerPage}&page=${page}`,
};

@Injectable({
  providedIn: 'root'
})
export class EpisodeService {

  constructor(private http: HttpClient) {}

  index(): Observable<any> {
	  return this.http.get<any>(routes.index)
	  .pipe(
        map((response: any) => {
          const data = response as Array<Episode>;

          return data;
        })
      );
  }

  paginate(itemsPerPage: number, page: number): Observable<any> {
    return this.http.get<any>(routes.paginate(itemsPerPage, page))
    .pipe(
        map((response: any) => {
          const data = response.data as Array<Episode>;

          return data;
        })
     );
  }
}
