import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Status } from '../types/status';

const routes = {
  index: 'statuses',
};

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  constructor(private http: HttpClient) {}

  index(): Observable<any> {
	return this.http.get<any>(routes.index)
	  .pipe(
        map((response: any) => {
          const data = response.data as Array<Status>;
          return data;
        })
      );
  }
}
