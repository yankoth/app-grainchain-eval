import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Character } from '../types/character';

const routes = {
  index: 'characters',
  paginate: (itemsPerPage: number, page: number) =>  `characters?items_per_page=${itemsPerPage}&page=${page}`,
  update: (id: number) =>  `characters/${id}`,
};

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private http: HttpClient) {}

  index(): Observable<any> {
	  return this.http.get<any>(routes.index)
	  .pipe(
        map((response: any) => {
          const data = response as Array<Character>;

          return data;
        })
     );
  }

  paginate(itemsPerPage: number, page: number): Observable<any> {
    return this.http.get<any>(routes.paginate(itemsPerPage, page))
    .pipe(
        map((response: any) => {
          const data = response.data as Array<Character>;

          return data;
        })
     );
  }

  update (character: Character) {
    return this.http.put<any>(routes.update(character.id), character)
    .pipe(
        map((response: any) => {
          const data = response as Character;

          return data;
        })
     );
  }
}
