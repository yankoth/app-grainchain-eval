import { Status } from './status';
import { Gender } from './gender';
import { Location } from './location';

export class Character {
  constructor (
    public id: number = 0,
    public name: string = '',
    public status: Status = new Status(),
    public species: string = '',
    public type: string = '',
    public gender: Gender = new Gender(),
    public origin: string = '',
    public image: string = '',
    public location: Location = new Location(),
  ) { }
}