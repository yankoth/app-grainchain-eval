export class Location {
  constructor (
  	public id: number = 0,
    public name: string = '',
    public type: string = '',
    public dimension: string = '',
    public url: string = '',
  ) { }
}