import { Character } from './character';

export class Episode {
  constructor (
  	public id: number = 0,
  	public season: number = 0,
    public number: number = 0,
    public name: string = '',
    public air_date: Date = new Date('0001-01-01'),
    public characters: Array<Character> = [],
  ) { }
}