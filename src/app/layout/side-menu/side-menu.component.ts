import { Component, OnInit } from '@angular/core';

import { AuthService } from '@data/service/auth.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  menu: Array<string> = ['characters', 'episodes', 'auth/logout'];

  constructor(private authService: AuthService) { }

  get isAuthenticated(): boolean { return this.authService.isAuthenticated(); }

  ngOnInit(): void {
  }

  formatMenuItem (value: string) {
    const arr = value.split('/');
    const item = arr[arr.length - 1];

    return item[0].toUpperCase() + item.substr(1);
  }

}
