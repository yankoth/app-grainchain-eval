import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { LoadingState } from '@shared/components/loading/loading-state.model'

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  private loadingSubject = new Subject<LoadingState>();
  loadingState = this.loadingSubject.asObservable();

  constructor()  { }

  loadingOn() {
    this.loadingSubject.next(<LoadingState>{ isLoading: true });
  }

  loadingOff() {
    this.loadingSubject.next(<LoadingState>{ isLoading: false });
  }
}
