import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, interval } from 'rxjs';

import { LoadingService } from '@shared/services/loading.service';
import { LoadingState } from './loading-state.model';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnDestroy {

  loading: boolean = false;

  private loadingSubscription: Subscription | any = null;

  constructor(private loadingService: LoadingService) { }

  ngOnInit(): void {
    this.loadingSubscription = this.loadingService.loadingState
    .subscribe((state: LoadingState) => {
      this.loading = state.isLoading;

      if (this.loading) {
        let el = document.querySelector(':focus');

        if (el && el instanceof HTMLElement) {
          el.blur();
        }

        setTimeout(() => {
          let input = document.getElementById('loading-input');

          if (input && input instanceof HTMLElement) {
            input.focus();
          }
        }, 100);
      }
    });
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }

  keydown (event: any) {
    event.preventDefault();
  }
}
