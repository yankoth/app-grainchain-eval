import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss']
})
export class BadgeComponent implements OnInit {

  @Input() text: string = '';

  randomColor: string;
  whiteOrBlack: string;

  constructor() {
  	this.randomColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
  	this.whiteOrBlack = (parseInt(this.randomColor.replace('#', ''), 16) > 0xffffff / 2) ? 'black' : 'white';
  }

  ngOnInit(): void {
  }

}
