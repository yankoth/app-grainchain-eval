import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoadingComponent } from './components/loading/loading.component';
import { AlertComponent } from './components/alert/alert.component';
import { BadgeComponent } from './components/badge/badge.component';

@NgModule({
  declarations: [LoadingComponent, AlertComponent, BadgeComponent],
  imports: [
    CommonModule
  ],
  exports: [LoadingComponent, AlertComponent, BadgeComponent]
})
export class SharedModule { }
